package model;
//todos que quiserem ser tributável precisam saber calcular tributos, devolvendo um double

public interface Tributavel {
	double calculaTributos();
}