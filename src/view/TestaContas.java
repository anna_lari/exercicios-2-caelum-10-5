package view;

import model.Conta;
import model.ContaCorrente;
import model.ContaPoupanca;

public class TestaContas {
	public static void main(String[] args) {
		Conta conta = new ContaCorrente();
		ContaCorrente cc = new ContaCorrente();
		ContaPoupanca cp = new ContaPoupanca();

		conta.deposita(1000);
		cc.deposita(1000);
		cp.deposita(1000);

		conta.atualiza(0.10);
		cc.atualiza(0.10);
		cp.atualiza(0.10);
		

		System.out.println(conta.getSaldo());
		System.out.println(cc.getSaldo());
		System.out.println(cp.getSaldo());

	}
}